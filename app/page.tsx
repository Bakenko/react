import React from 'react';
import {Header, Logo, Menu, MenuSection} from '@/components/imports/Header';
import {InitialSection} from '@/components/imports/InitialSection';
import {LogoSponsor, Sponsors} from '@/components/imports/Sponsors';
import {BackgroundSection, Button, ButtonIcon, Container, Paragraph, Subtitle, Title, Line, FloatingButton} from '@/components/imports/GlobalComponents';
import {BoxContainer, FloatingBox, HorizontalBox, ImageHB, TextBox, Image, VerticalBox} from '@/components/imports/Boxes';
import {BoxFooter, Element, ElementsBox, SectionFooter, Footer, DropdownList, List} from '@/components/imports/Footer';
import {MdPlayCircleFilled, MdOutlineFilter, MdOutlineMemory, MdOutlineChecklistRtl, MdOutlineGppGood} from 'react-icons/md';
import {Warrior01, Warrior02, Warrior03, Warrior04, Warrior05, Warrior06, Warrior07, Warrior08, Warrior09, Warrior10} from '@/components/images';

export default function Home() {
  return (
    <main>
      <Header/>
      <BackgroundSection image='https://cdna.artstation.com/p/assets/images/images/055/233/436/large/z-w-gu-f11-battle.jpg?1666447842'>
        <InitialSection
          textPu='UNITI PARA JUEGOS'
          textT='CONTIGO DE LA GRAN IDEA AL ÉXITO'
          textPd='Haz realidad tu visión con Create Solutions de Unity, que es líder en el sector, y haz crecer tu empresa con Operate Solutions, que funciona con cualquier motor de juego.'
          nameB='Crea tu juego'
          nameT='Administra tu juego'
        />
      </BackgroundSection>
      <Menu/>
      <Container>
        <Subtitle 
            text='Soluciones para juegos'
        />
        <BoxContainer>
          <VerticalBox 
              src='https://cdnb.artstation.com/p/assets/images/images/055/234/173/large/z-w-gu-bg2-1d.jpg?1666449375'
              alt='warrior1'
              textS='Crea tu juego'
              textP='Haz realidad tu visión con las herramientas de Unity, que son líderes en la industria, para crear experiencias de juego increíbles y de alto rendimiento.'
              name='Más información'
              color='rgb(33, 150, 243)'
              colorT='black'
          />
          <VerticalBox 
              src='https://cdna.artstation.com/p/assets/images/images/055/234/314/large/z-w-gu-bg2-2d.jpg?1666449656'
              alt='warrior2'
              textS='Crea, administra y haz crecer tu juego'
              textP='Desarrolla juegos multijugador y multiplataforma, administra operaciones en vivo y crea experiencias personalizadas para tus jugadores con los servicios para videojuegos de Unity.'
              name='Más información'
              color='rgb(33, 150, 243)'
              colorT='black'
          />  
        </BoxContainer>
      </Container>
      <BackgroundSection image='https://cdnb.artstation.com/p/assets/images/images/055/233/607/large/z-w-gu-bg1-5e.jpg?1666448194'>
        <Container>
          <Subtitle 
              text='Nuestras opciones de plan'
              color='white'
          />
          <Paragraph 
            text='Más funcionalidad y recursos para potenciar tus juegos.'
            color='white'
          />
          <BoxContainer>
            <VerticalBox 
                src='https://cdna.artstation.com/p/assets/images/images/055/234/972/large/z-w-gu-bg2-8d.jpg?1666451085'
                alt='warrior3'
                textS='Unity Plus'
                textP='Los entusiastas serios y las empresas pequeñas obtienen más funcionalidad y recursos de capacitación para potenciar sus proyectos.'
                name='Más información'
                color='rgb(33, 150, 243)'
                colorT='black'
            />
            <VerticalBox 
                src='https://cdna.artstation.com/p/assets/images/images/055/233/504/large/z-w-gu-bg1-4c.jpg?1666447987'
                alt='warrior4'
                textS='Unity Pro'
                textP='Descubre el potencial de tu equipo con herramientas profesionales para crear juegos en dispositivos y plataformas.'
                name='Más información'
                color='rgb(33, 150, 243)'
                colorT='black'
            />
            <VerticalBox 
                src='https://cdnb.artstation.com/p/assets/images/images/055/234/447/large/z-w-gu-bg2-3e.jpg?1666449861'
                alt='warrior5'
                textS='Unity Enterprise'
                textP='Administra complejos proyectos 3D en tiempo real con soporte experto y herramientas de creación que se pueden ampliar para equipos de cualquier tamaño.'
                name='Más información'
                color='rgb(33, 150, 243)'
                colorT='black'
            />  
          </BoxContainer>
        </Container>
      </BackgroundSection>
      <Container>
        <Subtitle 
          text='El 50 % de los juegos para dispositivos se crearon con Unity'
        />
        <Sponsors>
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/riot-360x192%401x.jpg?itok=4Uf4BmkS'
            alt='RIOT'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/respawn-entertainment-360x192%401x.jpg?itok=e-2TBTdk'
            alt='RESPAWN ENTERTAINMENT'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/505games-360x192%401x.jpg?itok=_qNKCJP6'
            alt='505 GAMES'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/hi-rez-360x192%401x.jpg?itok=C9oxG9gV'
            alt='HI-RES STUDIO'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-11/uken%20games%20bw.jpg?itok=RWsKfA9f'
            alt='UKEN GAMES'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-11/Romero%20Games%20bw.jpg?itok=B9aXoUcj'
            alt='ROMERO GAMES'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/atari-360x192%401x.jpg?itok=NFO2Y8wV'
            alt='ATARI'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/madfinger-games-360x192%401x.jpg?itok=1hdgtiRg'
            alt='MADFINGER GAMES.COM'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/phoenixlabs-360x192%401x.jpg?itok=ljjlqYt1'
            alt='PHOENIX LABS'
          />
          <LogoSponsor 
            src='https://unity.com/sites/default/files/styles/sponsor/public/2021-04/wooga-360x192%401x.jpg?itok=RdA8RvFe'
            alt='WOOGA'
          />
        </Sponsors>
      </Container>
      <Container>
        <HorizontalBox>
          <TextBox 
            textS='Unity Pro y Unity Enterprise evolucionaron'
            textP='Los dos planes ahora incluirán Unity Mars y Havok Physics para Unity. Unity Enterprise ofrece soporte rápido, acceso al código fuente, versión del soporte a largo plazo (LTS) extendido y mucho más.'
            nameB='Obtén más información sobre Unity Pro'
            nameT='Obtén más información dobre Unity Enterprise'
            colorT='black'
            colorB='rgb(33, 150, 243)'
          />
          <ImageHB 
            src='https://cdna.artstation.com/p/assets/images/images/028/138/058/large/z-w-gu-bandageb5f.jpg?1593594749'
            alt='Warrior6'
          />
        </HorizontalBox>
      </Container>
      <Container>
        <HorizontalBox>
          <ImageHB 
            src='https://cdna.artstation.com/p/assets/images/images/028/138/090/large/z-w-gu-witchrocks2epst.jpg?1593594811'
            alt='Warrior7'
          />
          <TextBox 
            textS='Games Focus: La serie'
            textP='En nuestra serie de blog Games Focus, los líderes y equipos de desarrolladores de diversas compañías comparten los proyectos en los que están trabajando, lo que tú puedes esperar de nosotros, y lo que eso significa para ti y tus proyectos.'
            nameB='Más información'
            colorT='black'
            hidden='none'
          />
        </HorizontalBox>
      </Container>
      <Container>
        <FloatingBox 
          textS='Crea y crece con Unity'
          textP='Unity es el motor de videojuegos líder del sector y mucho más. Descubre soluciones que te ayudarán en cada etapa del ciclo de vida del desarrollo del juego: de una gran idea a un éxito rotundo.'
          colorT='white'
          name='Más información'
        />
      </Container>
      <Footer>
        <Container size='0'>
          <SectionFooter sizeT='0' sizeB='0'>
            <BoxFooter size='50'>
              <Paragraph 
                text='Idioma'
                color='white'
              />
              <ElementsBox>
                <Element
                  text='English'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='Deutsch'
                  color='rgb(153, 153, 153)'
                />
                <Element 
                  text='日本語'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='Français'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='Português'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='中文'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='Español'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='Русский'
                  color='rgb(153, 153, 153)'
                />
                <Element
                  text='한국어'
                  color='rgb(153, 153, 153)'
                />
              </ElementsBox>
            </BoxFooter>
            <BoxFooter size='25'>
              <Paragraph 
                text='Redes sociales'
                color='white'
              />
              <ElementsBox gap='45'>
                <ButtonIcon 
                  icon={MdPlayCircleFilled}
                  size={24}
                  color='rgb(153, 153, 153)'
                 />
                 <ButtonIcon 
                  icon={MdOutlineFilter}
                  size={24}
                  color='rgb(153, 153, 153)'
                 />
                 <ButtonIcon 
                  icon={MdOutlineMemory}
                  size={24}
                  color='rgb(153, 153, 153)'
                 />
                 <ButtonIcon 
                  icon={MdOutlineChecklistRtl}
                  size={24}
                  color='rgb(153, 153, 153)'
                 />
                 <ButtonIcon 
                  icon={MdOutlineGppGood}
                  size={24}
                  color='rgb(153, 153, 153)'
                 />
              </ElementsBox>
            </BoxFooter>
            <BoxFooter size='25'>
              <Paragraph 
                text='Currency'
                color='white'
              />
              <DropdownList/>
            </BoxFooter>
          </SectionFooter>
          <Line/>
          <SectionFooter >
            <List />
          </SectionFooter>
          <SectionFooter sizeT='0' sizeB='0'>
            <BoxFooter size='100'>
              <Paragraph 
                text='Copyright © 2023 Unity Technologies'
                color='white'
              />
              <ElementsBox>
                  <Element
                    text='Legal'
                    color='rgb(153, 153, 153)'
                    size='100'
                  />
                  <Element
                    text='Privacy Policy'
                    color='rgb(153, 153, 153)'
                    size='150'
                  />
                  <Element
                    text='Cookies'
                    color='rgb(153, 153, 153)'
                    size='100'
                  />
                  <Element
                    text='Do Not Sell of Share My Personal Information'
                    color='rgb(153, 153, 153)'
                    size='400'
                  />
                  <Element
                    text='Configuarión de Cookies'
                    color='rgb(153, 153, 153)'
                    size='250'
                  />
                </ElementsBox>
            </BoxFooter>
          </SectionFooter>
          <SectionFooter sizeT='0' sizeB='0'>
            <BoxFooter>
                <Paragraph 
                  text='Unity, los logotipos de Unity y otras marcas comerciales de Unity son marcas comerciales o marcas comerciales registradas de Unity Technologies o de sus empresas afiliadas en los Estados Unidos y el resto del mundo (más información). Los demás nombres o marcas son marcas comerciales de sus respectivos propietarios.'
                  color='white'
                />
            </BoxFooter>
          </SectionFooter>
        </Container>
      </Footer>
      <FloatingButton/>
    </main>
  )
}
