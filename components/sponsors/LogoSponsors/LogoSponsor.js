import React from "react";
import styleSponsors from './stylesLogoSponsors.css';

const LogoSponsor = ({ src, alt }) => {
    return <div className="flex boxImage jCenter">
        <img className="imageSize" src={src} alt={alt} />
    </div>
};

export default LogoSponsor;