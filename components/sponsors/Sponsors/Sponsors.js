import React from "react";
import styleSponsors from './stylesSponsors.css';

function Sponsors ({ children }) {
    return <div className="flex boxLogo">
        {children}
    </div>
};

export default Sponsors;