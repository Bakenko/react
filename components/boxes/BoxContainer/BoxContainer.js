import React from "react";

function BoxContainer ({children}) {
    return <div className="w100 flex jCenter">
        {children}
    </div>
};

export default BoxContainer;