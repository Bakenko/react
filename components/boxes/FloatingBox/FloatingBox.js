import React from "react";
import BackgroundSection from '../../globalComponents/BackgroundSection/BackgroundSection'
import Subtitle from "@/components/globalComponents/Subtitle/Subtitle";
import Paragraph from "@/components/globalComponents/Paragraph/Paragraph";
import Button from "@/components/globalComponents/Button/Button";
import stylesFloatingBox from './stylesFloatingBox.css';

function FloatingBox ({ textS, textP, name, colorT}) {
    return <BackgroundSection image='https://cdnb.artstation.com/p/assets/images/images/055/234/721/large/z-w-gu-bg2-6d.jpg?1666450404' border='8'>
            <div className="flex w100 paddingBF">
                <div className="flex colum w80 gap50">
                    <Subtitle 
                        text={textS}
                        color={colorT}
                    />
                    <Paragraph 
                        text={textP}
                        color={colorT}
                    />
                    <div className="flex">
                        <button className="flex buttonBlue border">
                            <Button 
                                name={name}
                            />
                        </button>
                    </div>
                </div>
            </div>
    </BackgroundSection>
};

export default FloatingBox;