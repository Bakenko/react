import React from "react";
import styleImage from './styleImage.css';

const Image = ({ src, alt, borderT, borderB }) => {
    const borderImage ={
        borderTopLeftRadius: `${borderT}px`,
        borderTopRightRadius: `${borderT}px`,
        borderBottomLeftRadius: `${borderB}px`,
        borderBottomRightRadius: `${borderB}px`,
    }
    return <img className="styleImage imageCover" src={src} alt={alt} style={borderImage} />
};

export default Image;