import React from "react";
import Image from '../Image/Image';
import Subtitle from '../../globalComponents/Subtitle/Subtitle';
import Paragraph from '../../globalComponents/Paragraph/Paragraph';
import Button from '../../globalComponents/Button/Button';
import styleVerticalBox from './stylesVerticalBox.css';

function VerticalBox ({ src, alt, textS, textP, name, colorT, color, borderT, borderB }) {
    return <div className="w100 flex colum bSizing boxShadowV h100 backgroundVB vGap24">
        <Image 
            src={src}
            alt={alt}
            borderT='8'
            borderB='0'
        />
        <div className="flex colum w80 vGap24 aScenter">
            <Subtitle 
                text={textS}
                color={colorT}
            />
            <Paragraph 
                text={textP}
                color={colorT}
            />
            <div>
                <Button
                    name={name}
                    color={color}                                
                />
            </div>
        </div>
    </div>
};

export default VerticalBox;