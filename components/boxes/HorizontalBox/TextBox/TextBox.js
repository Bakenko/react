import React from "react";
import Subtitle from '../../../globalComponents/Subtitle/Subtitle';
import Paragraph from '../../../globalComponents/Paragraph/Paragraph';
import Button from '../../../globalComponents/Button/Button';
import styleTextBox from './styleTextBox.css';

function TextBox ({ textS, textP, nameB, nameT, colorT, color, colorB, hidden }) {
    const styleHidden = {
        display: `${hidden}`
    };
    return <div className="flex colum w50 gap50 p50">
        <div className="flex colum w80 gap50">
            <Subtitle 
                text={textS}
                color={colorT}
            />
            <Paragraph 
                text={textP}
                color={colorT}
            />
        </div>
        <div className="flex">
            <button className="button buttonUnityPro">
                <Button 
                    name={nameB}
                    color={color}
                />
            </button>
            <button className="button buttonUnityEnterprise" style={styleHidden}>
                <Button 
                    name={nameT}
                    color={colorB}
                />
            </button>
        </div>
    </div>
};

export default TextBox;