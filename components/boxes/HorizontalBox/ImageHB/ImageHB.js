import React from "react";
import Image from '../../Image/Image';
import styleImageHB from './styleImageHB.css';

function ImageHB ({ src, alt }) {
    return <div className="flex w50">
            <Image 
                src={src}
                alt={alt}
                borderT='8'
                borderB='8'
            />
        </div>
};

export default ImageHB;