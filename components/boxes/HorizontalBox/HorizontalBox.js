import React, { Children } from "react";
import stylesHorizontalBox from './stylesHorizontalBox.css';

function HorizontalBox ({ children }) {
    return <div className="flex w100">
        {children}
    </div>
};

export default HorizontalBox;