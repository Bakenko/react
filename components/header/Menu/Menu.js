import React from "react";
import MenuSection from '../MenuSection/MenuSection';
import Button from '../../globalComponents/Button/Button';
import stylesMenu from './stylesMenu.css'

function Menu () {
    return <section className="flex w100 header">
        <div className="w80 flex">
            <MenuSection>
                <Button name='Infomración general'/>
                <Button name='Opciones del plan'/>
            </MenuSection>
        </div>        
    </section>
};

export default Menu;