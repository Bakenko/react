import React from 'react';
import MenuSection from '../MenuSection/MenuSection';
import Button from '../../globalComponents/Button/Button';
import Logo from '../LogoHeader/Logo';
import stylesHeader from './stylesHeader.css';
import ButtonIcon from '../../globalComponents/ButtonIcon/ButtonIcon'
import { MdSearch, MdList, MdAccountCircle} from 'react-icons/md';

function Header () {
    return <header className='flex aCenter w100 header fixed'>
        <div className='w80 flex'>
            <MenuSection>
                <Logo 
                    src='https://images.contentstack.io/v3/assets/blt08c1239a7bff8ff5/bltb81e11ffb7512f4a/646666241514066ed7f31404/header-logo-mobile.svg' 
                    alt='Logo'
                />
                <Button name='Juegos'/>
                <Button name='Cine'/>
                <Button name='Industria'/>
                <Button name='Comunidad'/>
                <Button name='Recursos'/>
                <Button name='Learn'/>
            </MenuSection>
            <MenuSection>
                <div className='w100 flex aCenter jRight'>
                    <button className='buttonPyA'>
                        <Button name='PLANES Y PRECIOS'/>
                    </button>
                    <div className='flex jSpace gap20'>
                        <ButtonIcon 
                            icon={MdSearch}
                            size={24}
                            color='white'
                        />
                        <ButtonIcon 
                            icon={MdList}
                            size={24}
                            color='white'
                        />
                        <ButtonIcon 
                            icon={MdAccountCircle}
                            size={24}
                            color='white'
                        />
                    </div>
                </div>
            </MenuSection>
        </div>
        
    </header>
};

export default Header;