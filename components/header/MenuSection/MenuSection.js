import React from "react";
import ButtonMenu from '../../globalComponents/Button/Button';
import Logo from '../LogoHeader/Logo'

function MenuSection ( {children}) {
    return <div className="w50 flex aCenter">
        {children}
    </div>
};

export default MenuSection;