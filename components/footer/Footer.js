import React from "react";
import stylesFooter from './stylesFooter.css';

function Footer ({ children }) {
    return <section className="flex colum w100 backgroundFooter">
        {children}
    </section>
};

export default Footer;