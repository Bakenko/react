import React from 'react';
import stylesSectionFooter from './stylesSectionFooter.css';

function SectionFooter ({children, sizeT, sizeB}) {
    const stylePadding = {
        paddingTop: `${sizeT}px`,
        paddingButton: `${sizeB}px`
    }
    return <div className='flex w100 jSpace paddingSection gap50' style={stylePadding}>
            {children}
        </div>
};

export default SectionFooter;