import React from "react";
import Paragraph from '../../globalComponents/Paragraph/Paragraph'
import styleElement from './stylesElement.css';

const Element = ({ text, color, size }) => {
    const sizeElement = {
        width: `${size}px`
    }
    return <div className="flex elementText" style={sizeElement}>
        <Paragraph 
            text={text}
            color={color}
        />
    </div>
};

export default Element;