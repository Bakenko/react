import React from "react";
import styleDropdownList from './styleDropdownList.css'

function DropdownList ({}) {
    return <select className="styleDropdownList">
        <option>BRL</option>
        <option>CNY</option>
        <option>JPY</option>
        <option>KWR</option>
        <option>USD</option>
        <option>EUR</option>
    </select>
};

export default DropdownList;