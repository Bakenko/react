import React from "react";
import styleLanguageBox from './stylesLanguageBox.css';

function ElementsBox ({ children, gap }) {
    const elmentsGap = {
        gap: `${gap}px`
    }
    return <div className="flex boxLanguage" style={elmentsGap}>
        {children}
    </div>
};

export default ElementsBox;