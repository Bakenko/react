import React from "react";
import styleBoxFooter from './styleBoxFooter.css'

function BoxFooter ({ children, size }) {
    const sizeBox = {
        width: `${size}%`
    }

    return <div className="flex colum gap16" style={sizeBox}>
        {children}
    </div>
};

export default BoxFooter;