import React from "react";
import BoxFooter from '../footer/boxFooter/BoxFooter';
import Element from '../footer/element/Element';
import ElementsBox from '../footer/ElementsBox/ElementsBox';
import SectionFooter from '../footer/SectionFooter/SectionFooter';
import Footer from '../footer/Footer';
import DropdownList from '../footer/DropdownList/DropdownList';
import List from '../footer/Lists/List';

export {
    BoxFooter,
    Element,
    ElementsBox,
    SectionFooter,
    Footer,
    DropdownList,
    List
}