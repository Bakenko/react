import React from "react";
import BoxContainer from '../boxes/BoxContainer/BoxContainer';
import FloatingBox from '../boxes/FloatingBox/FloatingBox';
import HorizontalBox from '../boxes/HorizontalBox/HorizontalBox';
import ImageHB from '../boxes/HorizontalBox/ImageHB/ImageHB';
import TextBox from '../boxes/HorizontalBox/TextBox/TextBox';
import Image from '../boxes/Image/Image';
import VerticalBox from '../boxes/VerticalBox/VerticalBox';

export {
    BoxContainer,
    FloatingBox,
    HorizontalBox,
    ImageHB,
    TextBox,
    Image,
    VerticalBox
}