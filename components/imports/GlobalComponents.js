import React from "react";
import BackgroundSection from '../globalComponents/BackgroundSection/BackgroundSection';
import Button from '../globalComponents/Button/Button';
import ButtonIcon from '../globalComponents/ButtonIcon/ButtonIcon';
import Container from '../globalComponents/Container/Container';
import Paragraph from '../globalComponents/Paragraph/Paragraph';
import Subtitle from '../globalComponents/Subtitle/Subtitle';
import Title from '../globalComponents/Title/Title';
import Line from '../globalComponents/HorizontalLine/Line';
import FloatingButton from "../globalComponents/floatingButton/FloatingButton";

export {
    BackgroundSection,
    Button,
    ButtonIcon,
    Container,
    Paragraph,
    Subtitle,
    Title,
    Line,
    FloatingButton
}