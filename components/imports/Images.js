import React from "react";
import Warrior01 from '../images/Warrior01.jpg';
import Warrior02 from '../images/Warrior02.jpg';
import Warrior03 from '../images/Warrior03.jpg';
import Warrior04 from '../images/Warrior04.jpg';
import Warrior05 from '../images/Warrior05.jpg';
import Warrior06 from '../images/Warrior06.jpg';
import Warrior07 from '../images/Warrior07.jpg';
import Warrior08 from '../images/Warrior08.jpg';
import Warrior09 from '../images/Warrior09.jpg';
import Warrior10 from '../images/Warrior10.jpg';

export default {
    Warrior01,
    Warrior02,
    Warrior03,
    Warrior04,
    Warrior05,
    Warrior06,
    Warrior07,
    Warrior08,
    Warrior09,
    Warrior10
}