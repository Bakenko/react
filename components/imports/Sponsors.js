import React from "react";
import LogoSponsor from '../sponsors/LogoSponsors/LogoSponsor';
import Sponsors from '../sponsors/Sponsors/Sponsors';

export {
    LogoSponsor,
    Sponsors
}