import React from 'react';
import stylesTitle from './stylesTitle.css';

const Title = ({text, color}) => {
    const colorTitle = {
        color: color
    }
    return <h1 className='title' style={colorTitle}>{text}</h1>
};

export default Title;