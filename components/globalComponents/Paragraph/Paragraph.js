import React from 'react';
import stylesParagraph from './stylesParagraph.css';

const Paragraph = ({text, color}) => {
    const colorParagraph = {
        color: color
    }
    return <p className='paragraph' style={colorParagraph}>{text}</p>
};

export default Paragraph;