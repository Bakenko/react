import React from "react";
import ButtonIcon from '../ButtonIcon/ButtonIcon';
import {MdContactSupport} from 'react-icons/md';
import styleFloatingButton from './styleFloatingButton.css';

const FloatingButton = () => {
    return <section className="flex w100 jRight fixed">
        <div className="boxIcon">
            <ButtonIcon 
                icon={MdContactSupport}
                size={40}
                color='white'
            />
       </div>
    </section>
}

export default FloatingButton;