import React from 'react';
import stylesSubtitle from './stylesSubtitle.css';

const Subtitle = ({text, color}) => {
    const colorSubtitle = {
        color: color
    }
    return <h2 className='subtitle'style={colorSubtitle}>{text}</h2>
};

export default Subtitle;