import React from "react";
import stylesBackgroundSection from './stylesBackgroundSection.css';

function BackgroundSection ({children, image, border}) {
    const background = {
        backgroundImage: `url(${image})`,
        borderTopLeftRadius: `${border}px`,
        borderTopRightRadius: `${border}px`,
        borderBottomLeftRadius: `${border}px`,
        borderBottomRightRadius: `${border}px`,
    };
    return <section className="flex w100 jCenter backgroundStyle" style={background}>
        {children}
    </section>
};

export default BackgroundSection;