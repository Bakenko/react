import React from 'react';
import stylesButton from './stylesButton.css';

function Button ({ name }) {
    if (name != 'Primary' || name != 'Secondary') {
        name = 'Secondary';
    }
    return <button className={`button p16 btn${name}`} >{name}</button>
};

export default Button;