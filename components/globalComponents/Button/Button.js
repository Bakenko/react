import React from 'react';
import stylesButton from './stylesButton.css';

function Button ({ name, color }) {
    const colorButton = {
        color: color
    }
    return <button className='button p16' style={colorButton}>{name}</button>
};

export default Button;