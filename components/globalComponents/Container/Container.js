import React from 'react';
import stylesContainer from './stylesContainer.css';

function Container ({children, size}) {
    const styleGap ={
        gap: `${size}px`
    }
    return <section className='flex w100 jCenter'>
        <div className='flex colum w80 jSpace paddingContainer gap50' style={styleGap}>
            {children}
        </div>
    </section>
};

export default Container;