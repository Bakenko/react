import React from "react";
import Title from '@/components/globalComponents/Title/Title';
import Paragraph from '@/components/globalComponents/Paragraph/Paragraph';
import Button from '@/components/globalComponents/Button/Button';
import stylesInitialSection from './stylesInitialSection.css';

function InitialSection ({ textPu, textT, textPd, nameB, nameT}) {
    return <div className="flex w80">
            <div className="flex w80 paddingBox">
                <div className="w100 flex colum tWhite h60 gap50">
                    <Paragraph
                        text={textPu}
                    />
                    <Title
                        text={textT}
                    />
                    <Paragraph
                        text={textPd}
                    />
                    <div className="flex">
                        <button className="button buttonCreate">
                            <Button
                                name={nameB}
                            />
                        </button>
                        <button className="button buttonAdmin">
                            <Button
                                name={nameT}
                            />
                        </button>
                    </div>
                </div> 
            </div>
        </div>
};

export default InitialSection;